\contentsline {chapter}{\numberline {第{1}章}序論 }{1}
\contentsline {chapter}{\numberline {第{2}章}研究の背景と目的 }{2}
\contentsline {section}{\numberline {2.1}研究の背景}{2}
\contentsline {subsection}{\numberline {2.1.1}Augmented Realityにおけるトラッキング技術}{2}
\contentsline {subsection}{\numberline {2.1.2}ビジョンベースのリローカリゼーション手法}{3}
\contentsline {subsection}{\numberline {2.1.3}環境再構成モデルを用いたリローカリゼーション}{4}
\contentsline {section}{\numberline {2.2}研究の目的}{4}
\contentsline {chapter}{\numberline {第{3}章}環境再構成モデルを用いたデータベース構築手法の開発 }{5}
\contentsline {section}{\numberline {3.1}既存のリローカリゼーション手法とその問題点}{5}
\contentsline {section}{\numberline {3.2}環境再構成モデルを用いたリローカリゼーションの処理の流れ}{6}
\contentsline {subsection}{\numberline {3.2.1}環境再構成モデルの作成}{6}
\contentsline {subsection}{\numberline {3.2.2}キーフレームデータベースの構築}{7}
\contentsline {subsection}{\numberline {3.2.3}類似画像検索}{8}
\contentsline {section}{\numberline {3.3}キーフレームデータベース構築手法の提案}{8}
\contentsline {subsection}{\numberline {3.3.1}等間隔サンプリング法}{9}
\contentsline {subsection}{\numberline {3.3.2}全域ランダムサンプリング法}{10}
\contentsline {subsection}{\numberline {3.3.3}ランダムウォークサンプリング法}{10}
\contentsline {subsection}{\numberline {3.3.4}深度値標準偏差サンプリング法}{11}
\contentsline {chapter}{\numberline {第{4}章}提案手法の評価 }{13}
\contentsline {section}{\numberline {4.1}評価の目的と概要}{13}
\contentsline {section}{\numberline {4.2}評価の方法}{13}
\contentsline {subsection}{\numberline {4.2.1}評価に用いる指標}{13}
\contentsline {subsection}{\numberline {4.2.2}評価用データセットの作成}{15}
\contentsline {subsection}{\numberline {4.2.3}評価の手順}{15}
\contentsline {section}{\numberline {4.3}評価の結果と考察}{17}
\contentsline {subsection}{\numberline {4.3.1}キーフレームデータベースの構築時間}{17}
\contentsline {subsection}{\numberline {4.3.2}領域カバー率}{18}
\contentsline {subsection}{\numberline {4.3.3}リローカリゼーションの成功率}{18}
\contentsline {subsection}{\numberline {4.3.4}リローカリゼーションの所要時間}{19}
\contentsline {chapter}{\numberline {第{5}章}結論 }{20}
\contentsline {chapter}{謝 辞}{21}
\contentsline {chapter}{参 考 文 献}{22}
\contentsline {chapter}{\numberline {付 録{A} }Randomized Fernの詳細 }{24}
\contentsline {chapter}{\numberline {付 録{B} }2つのカメラ姿勢の差 }{26}
